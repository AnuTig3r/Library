��          �      l      �     �  	   �     �     �          !  	   8     B     G     W     \     d     i     o     |     �  	   �     �     �     �    �     �  
   �           	     !     8  	   W     a     i     ~     �     �     �     �     �  	   �     �     �     �              	                           
                                                              Author Available Document Document Categories Document Category Document was returned. Documents Lend Lend "%s" to %s Lent Library Loan Loans New Document New Document Category Past Due Reference Return Date Return document The document was lent. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-20 15:45+0100
PO-Revision-Date: 2019-02-20 15:46+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2;ndgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Auteur Disponible Document Catégories de Document Catégorie de Document Le document a été retourné. Documents Prêter Prêter "%s" à "%s" Prêté Bibliothèque Prêt Prêts Nouveau Document Nouvelle Catégorie de Document En retard Référence Date de Retour Retourner le document Le document est prêté. 