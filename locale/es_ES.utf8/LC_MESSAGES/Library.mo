��          �      l      �     �  	   �     �     �          !  	   8     B     G     W     \     d     i     o     |     �  	   �     �     �     �  3  �       
     	        '     A     Z  
   u     �     �     �  
   �     �  	   �     �     �     �  
   �               1         	                           
                                                              Author Available Document Document Categories Document Category Document was returned. Documents Lend Lend "%s" to %s Lent Library Loan Loans New Document New Document Category Past Due Reference Return Date Return document The document was lent. Project-Id-Version: Library module for RosarioSIS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-02-20 15:45+0100
PO-Revision-Date: 2019-02-20 15:45+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2;ngettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Autor Disponible Documento Categorías de Documentos Categoría de Documentos Se devolvió el documento. Documentos Prestar Prestar "%s" a "%s" Prestado Biblioteca Prestamo Prestamos Nuevo Documento Nueva Categoría de Documentos Aplazado Referencia Fecha de Devolución Devolver el documento El documento está prestado. 