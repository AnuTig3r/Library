Library Module
==============

![screenshot](https://gitlab.com/francoisjacquet/Library/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/library-module/

Version 1.3 - April, 2019

License GNU GPL v2

Author François Jacquet

DESCRIPTION
-----------
Library module for RosarioSIS. Manage your school library documents (book, magazine, CD, etc.) and lend them to students, parents and staff.
Documents are organized in categories. Loans history can be consulted.
The library is shared amongst schools within RosarioSIS.
Translated in French & Spanish.

Premium Library module (not available yet):
- Set a Color code for each document Category.
- Document Fields: add custom fields about your documents (type, ISBN, collection, editor, language, state, etc.)
- Upload and attach file (PDF, epub, MP3, etc).
- Search documents by author, collection, language, status, etc.
- Browse documents by Covers.
- Assign one or more schools to each document.
- Return Comment.
- Portal and email late / past due date notifications.
- Admin Dashboard integration.
- Loans report.


CONTENT
-------
Library
- Library
- Loans

INSTALL
-------
Copy the `Library/` folder (if named `Library-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 4.2+
