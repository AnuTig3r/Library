<?php
/**
 * Library common functions
 *
 * @package Library module
 */


/**
 * Truncate Title to 36 chars
 * for responsive display in List.
 * Full title is in tooltip.
 *
 * @see Can be called through DBGet()'s functions parameter
 *
 * @param  string $value  Title value.
 * @param  string $column 'TITLE' (optional). Defaults to ''.
 */
function LibraryTruncateTitle( $value, $column = '' )
{
	// Truncate value to 36 chars.
	$title = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . $value . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $title;
}



function LibraryDrawUserTypeHeader( $action = '' )
{
	if ( User( 'PROFILE' ) !== 'admin' )
	{
		return '';
	}

	$header = '<a href="Modules.php?modname=' . $_REQUEST['modname'] . '&type=student' . $action . '">' .
		( $_REQUEST['type'] === 'student' ?
			'<b>' . _( 'Students' ) . '</b>' : _( 'Students' ) ) . '</a>';

	$header .= ' | <a href="Modules.php?modname=' . $_REQUEST['modname'] . '&type=staff' . $action . '">' .
		( $_REQUEST['type'] === 'staff' ?
			'<b>' . _( 'Users' ) . '</b>' : _( 'Users' ) ) . '</a>';

	DrawHeader( $header );
}

function LibraryGetDocument( $document_id, $reset = false )
{
	static $documents = array();

	if ( $document_id < 1 )
	{
		return array();
	}

	if ( isset( $documents[ $document_id ] )
		&& ! $reset )
	{
		return $documents[ $document_id ];
	}

	$document_RET = DBGet( DBQuery( "SELECT ID,CATEGORY_ID,TITLE,REF,
		DESCRIPTION,AUTHOR,YEAR,CREATED_AT,CREATED_BY,
		(SELECT TITLE
			FROM LIBRARY_CATEGORIES
			WHERE ID=CATEGORY_ID) AS CATEGORY_TITLE
		FROM LIBRARY_DOCUMENTS
		WHERE ID='" . $document_id . /*"'
		AND SCHOOL_ID='" . UserSchool() . */"'" ) );

	$documents[ $document_id ] = ( ! $document_RET ? array() : $document_RET[1] );

	return $documents[ $document_id ];
}

function LibraryMakeDocumentAPA( $value, $column = 'DOCUMENT_ID' )
{
	global $THIS_RET;

	if ( isset( $THIS_RET['TITLE'] )
		&& isset( $THIS_RET['AUTHOR'] )
		&& isset( $THIS_RET['YEAR'] ) )
	{
		$document = $THIS_RET;
	}
	else
	{
		$document = LibraryGetDocument( $value );
	}

	if ( ! $document )
	{
		return '';
	}

	$apa = $document['TITLE'];

	if ( $document['AUTHOR']
		|| $document['YEAR'] )
	{
		$apa .= ' (';

		$apa .= $document['AUTHOR'] ? $document['AUTHOR'] : '';

		$apa .= $document['YEAR'] ? ', ' . $document['YEAR'] : '';

		$apa .= ')';
	}

	return $apa;
}


function LibraryDrawDocumentHeader( $document_id )
{
	$document = LibraryGetDocument( $document_id );

	if ( ! $document )
	{
		return;
	}

	$document_apa = LibraryMakeDocumentAPA( $document_id );

	$title = $document['CATEGORY_TITLE'] . ' - ';

	$title .= '<a href="Modules.php?modname=Library/Library.php&category_id=' .
		$document['CATEGORY_ID'] . '&id=' . $document['ID'] . '">' . $document_apa . '</a>';

	DrawHeader( $title );
}


/**
 * Get Document Lending Status
 *
 * @param int $document_id Document ID.
 *
 * @return string Empty if no document, else free|lent|late.
 */
function LibraryGetDocumentStatus( $document_id )
{
	if ( $document_id < 1 )
	{
		return false;
	}

	$document_lent_RET = DBGet( DBQuery( "SELECT DATE_DUE
		FROM LIBRARY_LOANS
		WHERE DOCUMENT_ID='" . (int) $document_id . "'
		AND DATE_RETURN IS NULL
		AND DATE_BEGIN<=CURRENT_DATE
		LIMIT 1" ) );

	$lent = (bool) $document_lent_RET;

	if ( ! $lent )
	{
		return 'free';
	}

	$late = $lent && ( $document_lent_RET[1]['DATE_DUE'] < DBDate() );

	return $late ? 'late' : 'lent';
}
