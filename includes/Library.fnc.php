<?php
/**
 * Library functions
 *
 * @package Library module
 */


/**
 * Get Document or Document Category Form
 *
 * @example echo GetDocumentsForm( $title, $RET );
 *
 * @example echo GetDocumentsForm(
 *              $title,
 *              $RET,
 *              null,
 *              array( 'text' => _( 'Text' ), 'textarea' => _( 'Long Text' ) )
 *          );
 *
 * @uses DrawHeader()
 * @uses MakeDocumentType()
 *
 * @param  string $title                 Form Title.
 * @param  array  $RET                   Document or Document Category Data.
 * @param  array  $extra_category_fields Extra fields for Document Category.
 * @param  array  $type_options          Associative array of Document Types (optional). Defaults to null.
 *
 * @return string Document or Document Category Form HTML
 */
function LibraryGetDocumentsForm( $title, $RET, $extra_category_fields = array(), $type_options = null )
{
	$id = empty( $RET['ID'] ) ? '' : $RET['ID'];

	$category_id = empty( $RET['CATEGORY_ID'] ) ? '' : $RET['CATEGORY_ID'];

	if ( empty( $id )
		&& empty( $category_id ) )
	{
		return '';
	}

	$new = $id === 'new' || $category_id === 'new';

	$form = '<form action="Modules.php?modname=' . $_REQUEST['modname'];

	if ( $category_id
		&& $category_id !== 'new' )
	{
		$form .= '&category_id=' . $category_id;
	}

	if ( $id
		&& $id !== 'new' )
	{
		$form .= '&id=' . $id;
	}

	if ( $id )
	{
		$full_table = 'LIBRARY_DOCUMENTS';
	}
	else
	{
		$full_table = 'LIBRARY_CATEGORIES';
	}

	$form .= '&table=' . $full_table . '" method="POST">';

	$allow_edit = AllowEdit();

	$div = $allow_edit;

	$delete_button = '';

	if ( $allow_edit
		&& ! $new
		&& ( $id || ! LibraryCategoryHasDocuments( $category_id ) ) )
	{
		$delete_URL = "'Modules.php?modname=" . $_REQUEST['modname'] .
			'&modfunc=delete&category_id=' . $category_id .
			'&id=' . $id . "'";

		$delete_button = '<input type="button" value="' . _( 'Delete' ) . '" onClick="ajaxLink(' . $delete_URL . ');" /> ';
	}

	ob_start();

	DrawHeader( $title, $delete_button . SubmitButton() );

	$form .= ob_get_clean();

	$header = '<table class="width-100p valign-top fixed-col cellpadding-5"><tr class="st">';

	if ( $id )
	{
		// FJ document name required.
		$header .= '<td>' . TextInput(
			( empty( $RET['TITLE'] ) ? '' : $RET['TITLE'] ),
			'tables[' . $id . '][TITLE]',
			_( 'Document' ),
			'required maxlength=1000' .
			( empty( $RET['TITLE'] ) ? ' size=35' : '' ),
			$div
		) . '</td>';

		$header .= '</tr><tr class="st">';

		// @todo Add TinyMCE Math plugin
		// @link https://stackoverflow.com/documents/20682820/inserting-mathematical-symbols-into-tinymce-4#20686520
		$header .= '<td colspan="2">' . TinyMCEInput(
			( empty( $RET['DESCRIPTION'] ) ? '' : $RET['DESCRIPTION'] ),
			'tables[' . $id . '][DESCRIPTION]',
			_( 'Description' )
		) . '</td>';

		$header .= '</tr><tr class="st">';

		// REFERENCE.
		if ( ! $new )
		{
			// You can't change a student document type after it has been created.
			$header .= '<td>' . NoInput(
				$RET['REF'],
				dgettext( 'Library', 'Reference' )
			) . '</td>';
		}
		else
		{
			$header .= '<td' . ( ! $category_id ? ' colspan="2"' : '' ) . '>' . TextInput(
				( empty( $RET['REF'] ) ? '' : $RET['REF'] ),
				'tables[' . $id . '][REF]',
				dgettext( 'Library', 'Reference' ),
				'maxlength=50 required',
				$div
			) . '</td>';
		}

		if ( $category_id )
		{
			// CATEGORIES.
			$categories_RET = DBGet( DBQuery( "SELECT ID,TITLE,SORT_ORDER
				FROM LIBRARY_CATEGORIES
				ORDER BY SORT_ORDER,TITLE" ) );

			foreach ( (array) $categories_RET as $category )
			{
				$categories_options[ $category['ID'] ] = $category['TITLE'];
			}

			$header .= '<td>' . SelectInput(
				$RET['CATEGORY_ID'] ? $RET['CATEGORY_ID'] : $category_id,
				'tables[' . $id . '][CATEGORY_ID]',
				_( 'Category' ),
				$categories_options,
				false,
				'required'
			) . '</td>';
		}

		$header .= '</tr><tr class="st">';

		$field = array(
			'ID' => 'AUTHOR',
			'TYPE' => 'autox',
			'SELECT_OPTIONS' => '',
			'REQUIRED' => 'Y',
		);

		// AUTHOR.
		$header .= '<td>' . LibraryCustomSelectInput(
			$field,
			( empty( $RET['AUTHOR'] ) ? '' : $RET['AUTHOR'] ),
			'tables[' . $id . '][AUTHOR]',
			dgettext( 'Library', 'Author' )
		) . '</td>';

		// YEAR.
		$header .= '<td>' . TextInput(
			( empty( $RET['YEAR'] ) ? date( 'Y' ) : $RET['YEAR'] ),
			'tables[' . $id . '][YEAR]',
			_( 'Year' ),
			'type="number" min="-9999" max="9999" size="4"',
			! $new
		) . '</td>';

		$header .= '</tr><tr class="st">';

		// @todo Premium module!
		$header .= LibraryCustomFieldsForm( $id );

		$header .= '</tr></table>';
	}
	// Documents Category Form.
	else
	{
		$title = isset( $RET['TITLE'] ) ? $RET['TITLE'] : '';

		// Title document.
		$header .= '<td>' . TextInput(
			$title,
			'tables[' . $category_id . '][TITLE]',
			_( 'Title' ),
			'required maxlength=255' . ( empty( $title ) ? ' size=20' : '' )
		) . '</td>';

		// Sort Order document.
		$header .= '<td>' . TextInput(
			( isset( $RET['SORT_ORDER'] ) ? $RET['SORT_ORDER'] : '' ),
			'tables[' . $category_id . '][SORT_ORDER]',
			_( 'Sort Order' ),
			'size=5'
		) . '</td>';

		// Extra Fields.
		if ( ! empty( $extra_category_fields ) )
		{
			$i = 2;

			foreach ( (array) $extra_category_fields as $extra_field )
			{
				if ( $i % 3 === 0 )
				{
					$header .= '</tr><tr class="st">';
				}

				$colspan = 1;

				if ( $i === ( count( $extra_category_fields ) + 1 ) )
				{
					$colspan = abs( ( $i % 3 ) - 3 );
				}

				$header .= '<td colspan="' . $colspan . '">' . $extra_field . '</td>';

				$i++;
			}
		}

		$header .= '</tr></table>';
	}

	ob_start();

	DrawHeader( $header );

	$form .= ob_get_clean();

	$form .= '</form>';

	return $form;
}

if ( ! function_exists( 'LibraryCustomFieldsForm' ) )
{
	// @todo Premium module.
	function LibraryCustomFieldsForm( $id )
	{
		return '';
	}
}


/**
 * Outputs Documents or Document Categories Menu
 *
 * @example DocumentsMenuOutput( $documents_RET, $_REQUEST['id'], $_REQUEST['category_id'] );
 * @example DocumentsMenuOutput( $categories_RET, $_REQUEST['category_id'] );
 *
 * @uses ListOutput()
 *
 * @param array  $RET         Document Categories (ID, TITLE, SORT_ORDER columns) or Documents (+ REF column) RET.
 * @param string $id          Document Category ID or Document ID.
 * @param string $category_id Document Category ID (optional). Defaults to '0'.
 */
function LibraryDocumentsMenuOutput( $RET, $id, $category_id = '0' )
{
	if ( $RET
		&& $id
		&& $id !== 'new' )
	{
		foreach ( (array) $RET as $key => $value )
		{
			if ( $value['ID'] == $id )
			{
				$RET[ $key ]['row_color'] = Preferences( 'HIGHLIGHT' );
			}
		}
	}

	$LO_options = array( 'save' => false, 'search' => false, 'responsive' => false );

	if ( ! $category_id )
	{
		$LO_columns = array(
			'TITLE' => _( 'Category' ),
		);
	}
	else
	{
		$LO_columns = array(
			'TITLE' => _( 'Document' ),
			'REF' => dgettext( 'Library', 'Reference' ),
		);
	}

	$LO_link = array();

	$LO_link['TITLE']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'];

	if ( $category_id )
	{
		$LO_link['TITLE']['link'] .= '&category_id=' . $category_id;
	}

	$LO_link['TITLE']['variables'] = array( ( ! $category_id ? 'category_id' : 'id' ) => 'ID' );

	$LO_link['add']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] . '&category_id=';

	$LO_link['add']['link'] .= $category_id ? $category_id . '&id=new' : 'new';

	// Fix Teacher cannot add new Library / not displaying Documents total.
	$tmp_allow_edit = false;

	if ( ! $category_id )
	{
		ListOutput(
			$RET,
			$LO_columns,
			dgettext( 'Library', 'Document Category' ),
			dgettext( 'Library', 'Document Categories' ),
			$LO_link,
			array(),
			$LO_options
		);
	}
	else
	{
		$LO_options['search'] = true;

		ListOutput(
			$RET,
			$LO_columns,
			dgettext( 'Library', 'Document' ),
			dgettext( 'Library', 'Documents' ),
			$LO_link,
			array(),
			$LO_options
		);
	}
}


/**
 * Category has Documents?
 *
 * @param int $category_id Documents Category ID.
 *
 * @return bool True if Category has Documents.
 */
function LibraryCategoryHasDocuments( $category_id )
{
	if ( $category_id < 1 )
	{
		return false;
	}

	$category_has_documents = DBGet( DBQuery( "SELECT 1
		FROM LIBRARY_DOCUMENTS
		WHERE CATEGORY_ID='" . (int) $category_id . /*"'
		AND SCHOOL_ID='" . UserSchool() . */"'
		LIMIT 1" ) );

	return (bool) $category_has_documents;
}


// Allow User to Lend if can Edit Loans & document can be lent!
function LibraryCanLendDocument( $document_id )
{
	if ( $document_id < 1 )
	{
		return false;
	}

	$status = LibraryGetDocumentStatus( $document_id );

	return $status !== 'lent'
		&& $status !== 'late'
		&& AllowEdit( 'Library/Loans.php' );
}


function LibraryGetDocumentLoansHeader( $RET )
{
	$header = '';

	$id = empty( $RET['ID'] ) ? '' : $RET['ID'];

	if ( ! $id
		|| $id === 'new' )
	{
		return $header;
	}

	$header = '';

	if ( AllowUse( 'Library/Loans.php' )
		&& User( 'PROFILE' ) === 'admin' )
	{
		// Only admins can browse Loans history per Document view.
		$header .= '<a href="Modules.php?modname=Library/Loans.php&document_id=' . $RET['ID'] . '"><b>' .
			dgettext( 'Library', 'Loans' ) . '</b></a>';
	}

	$header_right = '';

	$status = LibraryGetDocumentStatus( $RET['ID'] );

	$status_label = dgettext( 'Library', 'Lent' );

	if ( $status === 'late'
		&& User( 'PROFILE' ) === 'admin' )
	{
		$status_label = '<span style="color: red">' . dgettext( 'Library', 'Past Due' ) . '</span>';
	}

	if ( LibraryCanLendDocument( $RET['ID'] ) )
	{
		$header_right .= '<form action="Modules.php?modname=' . $_REQUEST['modname'] .
			'&id=' . $RET['ID'] . '&modfunc=lend" method="GET">';

		$header_right .= SubmitButton( dgettext( 'Library', 'Lend' ), '', '' );

		$header_right .= '</form>';
	}
	else
	{
		if ( $status === 'free' )
		{
			$status_label = '<span style="color: green">' . dgettext( 'Library', 'Available' ) . '</span>';
		}

		$header_right .= _( 'Status' ) . ': <b>' . $status_label . '</b>';
	}

	ob_start();

	DrawHeader( $header, $header_right );

	$author_header = ob_get_clean();

	return $author_header;
}


// See Schools.php for more Inputs.
function LibraryCustomSelectInput( $field, $value_custom, $name, $title = '' )
{
	$options = $select_options = array();

	$col_name = $field['ID'];

	if ( $field['SELECT_OPTIONS'] )
	{
		$options = explode(
			"\r",
			str_replace( array( "\r\n", "\n" ), "\r", $field['SELECT_OPTIONS'] )
		);
	}

	foreach ( (array) $options as $option )
	{
		$value = $option;

		// Exports specificities.
		if ( $field['TYPE'] === 'exports' )
		{
			$option = explode( '|', $option );

			$option = $value = $option[0];
		}
		// Codeds specificities.
		elseif ( $field['TYPE'] === 'codeds' )
		{
			list( $value, $option ) = explode( '|', $option );
		}

		if ( $value !== ''
			&& $option !== '' )
		{
			$select_options[$value] = $option;
		}
	}

	$div = true;

	// Get autos / edits pull-down edited options.
	if ( $field['TYPE'] === 'autos'
		|| $field['TYPE'] === 'autox'
		|| $field['TYPE'] === 'edits' )
	{
		$sql_options = "SELECT DISTINCT s." . $col_name . ",upper(s." . $col_name . ") AS SORT_KEY
			FROM LIBRARY_DOCUMENTS s
			WHERE s." . $col_name . " IS NOT NULL
			AND s." . $col_name . " != ''
			ORDER BY SORT_KEY";

		$options_RET = DBGet( DBQuery( $sql_options ) );

		if ( $value_custom === '---'
			|| count( $select_options ) <= 1
			&& empty( $options_RET ) )
		{
			// FJ new option.
			return TextInput(
				$value_custom === '---' ?
				array( '---', '<span style="color:red">-' . _( 'Edit' ) . '-</span>' ) :
				$value_custom,
				$name,
				$title,
				( $field['REQUIRED'] === 'Y' ? 'required' : '' ),
				$div
			);
		}

		// Add the 'new' option, is also the separator.
		$select_options['---'] = '-' . _( 'Edit' ) . '-';
	}

	foreach ( (array) $options_RET as $option )
	{
		$option_value = $option[$col_name];

		if ( $field['TYPE'] === 'autox' )
		{
			$select_options[$option_value] = $option_value;

			continue;
		}

		if ( ! isset( $select_options[$option_value] ) )
		{
			$select_options[$option_value] = '<span style="color:blue">' .
				$option_value . '</span>';
		}

		// Make sure the current value is in the list.
		if ( $value_custom != ''
			&& ! isset( $select_options[$value_custom] ) )
		{
			$select_options[$value_custom] = array(
				$value_custom,
				'<span style="color:' . ( $field['TYPE'] === 'autos' ? 'blue' : 'green' ) . '">' .
				$value_custom . '</span>',
			);
		}
	}

	return ChosenSelectInput(
		$value_custom,
		$name,
		$title,
		$select_options,
		( $field['REQUIRED'] ? false : 'N/A' ),
		( $field['REQUIRED'] ? ' required' : '' ),
		$div
	);
}
