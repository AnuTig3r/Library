<?php
/**
 * Library
 *
 * @package RosarioSIS
 * @subpackage modules
 */

require_once 'modules/Library/includes/common.fnc.php';
require_once 'modules/Library/includes/Library.fnc.php';

DrawHeader( ProgramTitle() );

$_REQUEST['category_id'] = empty( $_REQUEST['category_id'] ) ? '' : $_REQUEST['category_id'];
$_REQUEST['id'] = empty( $_REQUEST['id'] ) ? '' : $_REQUEST['id'];

if ( AllowEdit()
	&& isset( $_POST['tables'] )
	&& is_array( $_POST['tables'] ) )
{
	$table = isset( $_REQUEST['table'] ) ? $_REQUEST['table'] : null;

	require_once 'ProgramFunctions/MarkDownHTML.fnc.php';

	foreach ( (array) $_REQUEST['tables'] as $id => $columns )
	{
		if ( isset( $columns['DESCRIPTION'] ) )
		{
			$columns['DESCRIPTION'] = SanitizeHTML( $_POST['tables'][ $id ]['DESCRIPTION'] );
		}

		// FJ fix SQL bug invalid sort order.
		if ( empty( $columns['SORT_ORDER'] )
			|| is_numeric( $columns['SORT_ORDER'] ) )
		{
			// FJ added SQL constraint TITLE is not null.
			if ( ( ! isset( $columns['TITLE'] )
					|| ! empty( $columns['TITLE'] ) )
				&& ( ! isset( $columns['REF'] )
					|| ! empty( $columns['REF'] ) ) )
			{
				if ( ! empty( $columns['AUTHOR'] ) )
				{
					$columns['AUTHOR'] = trim( $columns['AUTHOR'] );
				}

				// Update Document / Category.
				if ( $id !== 'new' )
				{
					if ( isset( $columns['CATEGORY_ID'] )
						&& $columns['CATEGORY_ID'] != $_REQUEST['category_id'] )
					{
						$_REQUEST['category_id'] = $columns['CATEGORY_ID'];
					}

					$sql = 'UPDATE ' . $table . ' SET ';

					foreach ( (array) $columns as $column => $value )
					{
						$sql .= DBEscapeIdentifier( $column ) . "='" . $value . "',";
					}

					$sql = mb_substr( $sql, 0, -1 ) . " WHERE ID='" . $id . "'";

					$go = true;
				}
				// New Document / Category.
				else
				{
					$sql = 'INSERT INTO ' . $table . ' ';

					// New Document.
					if ( $table === 'LIBRARY_DOCUMENTS' )
					{
						if ( isset( $columns['CATEGORY_ID'] ) )
						{
							$_REQUEST['category_id'] = $columns['CATEGORY_ID'];

							unset( $columns['CATEGORY_ID'] );
						}

						// @deprecated use DBSeqNextID() since 4.5.
						$id_RET = DBGet( DBQuery( 'SELECT ' . db_seq_nextval( 'LIBRARY_DOCUMENTS_ID_SEQ' ) . ' AS ID ' ) );

						$_REQUEST['id'] = $id_RET[1]['ID'];

						$fields = 'ID,CATEGORY_ID,CREATED_BY,';

						$values = "'" . $_REQUEST['id'] . "','" . $_REQUEST['category_id'] . "','" . User( 'STAFF_ID' ) . "',";
					}
					// New Category.
					elseif ( $table === 'LIBRARY_CATEGORIES' )
					{
						// @deprecated use DBSeqNextID() since 4.5.
						$id_RET = DBGet( DBQuery( 'SELECT ' . db_seq_nextval( 'LIBRARY_CATEGORIES_ID_SEQ' ) . ' AS ID ' ) );

						$_REQUEST['category_id'] = $id_RET[1]['ID'];

						$fields = "ID,";

						$values = "'" . $_REQUEST['category_id'] . "',";
					}

					// School, Created by.
					/*$fields .= 'SCHOOL_ID,';

					$values .= "'" . UserSchool() . "',";*/

					$go = false;

					foreach ( (array) $columns as $column => $value )
					{
						if ( ! empty( $value )
							|| $value == '0' )
						{
							$fields .= DBEscapeIdentifier( $column ) . ',';

							$values .= "'" . $value . "',";

							$go = true;
						}
					}

					$sql .= '(' . mb_substr( $fields, 0, -1 ) . ') values(' . mb_substr( $values, 0, -1 ) . ')';
				}

				if ( $go )
				{
					DBQuery( $sql );
				}
			}
			else
				$error[] = _( 'Please fill in the required fields' );
		}
		else
			$error[] = _( 'Please enter valid Numeric data.' );
	}

	// Unset tables & redirect URL.
	RedirectURL( array( 'tables' ) );
}

// Delete Document / Category.
if ( $_REQUEST['modfunc'] === 'delete'
	&& AllowEdit() )
{
	if ( intval( $_REQUEST['id'] ) > 0 )
	{
		if ( DeletePrompt( dgettext( 'Library', 'Document' ) ) )
		{
			$delete_sql = "DELETE FROM LIBRARY_DOCUMENTS
				WHERE ID='" . $_REQUEST['id'] . /*"'
				AND SCHOOL_ID='" . UserSchool() . */"';";

			$delete_sql .= "DELETE FROM LIBRARY_LOANS
				WHERE DOCUMENT_ID='" . $_REQUEST['id'] . "';";

			DBQuery( $delete_sql );

			// Unset modfunc & ID & redirect URL.
			RedirectURL( array( 'modfunc', 'id' ) );
		}
	}
	elseif ( isset( $_REQUEST['category_id'] )
		&& intval( $_REQUEST['category_id'] ) > 0
		&& ! LibraryCategoryHasDocuments( $_REQUEST['category_id'] ) )
	{
		if ( DeletePrompt( dgettext( 'Library', 'Document Category' ) ) )
		{
			DBQuery( "DELETE FROM LIBRARY_CATEGORIES
				WHERE ID='" . $_REQUEST['category_id'] . /*"'
				AND SCHOOL_ID='" . UserSchool() . */"'" );

			// Unset modfunc & category ID redirect URL.
			RedirectURL( array( 'modfunc', 'category_id' ) );
		}
	}
}

// Lend Document to Student or Staff submit.
if ( $_REQUEST['modfunc'] === 'lend_submit' )
{
	if ( ! empty( $_REQUEST['id'] )
		&& LibraryCanLendDocument( $_REQUEST['id'] )
		&& ( UserStudentID() > 0 || UserStaffID() > 0 ) )
	{
		$user_id = $_REQUEST['type'] === 'student' ? ( UserStudentID() * -1 ) : UserStaffID();

		$requested_dates = RequestedDates(
			$_REQUEST['year_values'],
			$_REQUEST['month_values'],
			$_REQUEST['day_values']
		);

		$insert_sql = 'INSERT INTO LIBRARY_LOANS';

		$fields = 'DOCUMENT_ID,USER_ID,CREATED_BY,';

		$values = "'" . $_REQUEST['id'] . "','" . $user_id . "','" . User( 'STAFF_ID' ) . "',";

		$fields .= 'DATE_BEGIN,DATE_DUE,COMMENTS';

		$values .= "'" . $requested_dates['DATE_BEGIN'] . "','" .
			$requested_dates['DATE_DUE'] . "','" .
			$_REQUEST['values']['COMMENTS'] . "'";

		$insert_sql .= '(' . $fields . ') values(' . $values . ')';

		DBGet( DBQuery( $insert_sql ) );

		$note[] = button( 'check', '', '', 'bigger' ) .
			dgettext( 'Library', 'The document was lent.' );
	}

	// Unset modfunc & redirect URL.
	RedirectURL( array( 'modfunc', 'type' ) );
}


// Lend Document to Student or Staff view.
if ( $_REQUEST['modfunc'] === 'lend' )
{
	if ( ! empty( $_REQUEST['id'] )
		&& LibraryCanLendDocument( $_REQUEST['id'] ) )
	{
		$_REQUEST['type'] = ( isset( $_REQUEST['type'] ) && $_REQUEST['type'] === 'staff' ) ?
			'staff' :
			( User( 'PROFILE' ) === 'teacher' ? 'staff' : 'student' );

		LibraryDrawUserTypeHeader( '&modfunc=lend&id=' . $_REQUEST['id'] );
		LibraryDrawDocumentHeader( $_REQUEST['id'] );

		$extra = isset( $extra ) ? $extra : array();

		$extra['action'] = '&id=' . $_REQUEST['id'] . '&type=' . $_REQUEST['type'];

		$extra['link']['FULL_NAME']['link'] = 'Modules.php?modname=Library/Library.php' . $extra['action'] .
				'&modfunc=lend';

		if ( $_REQUEST['type'] === 'student' )
		{
			Search( 'student_id', $extra );
		}
		else
		{
			Search( 'staff_id', $extra );
		}

		if ( $_REQUEST['type'] === 'student' && UserStudentID()
			|| $_REQUEST['type'] === 'staff' && UserStaffID() )
		{
			echo '<br />';

			PopTable( 'header', dgettext( 'Library', 'Lend' ), 'style="max-width: 300px;"' );

			echo '<form action="Modules.php?modname=Library/Library.php' . $extra['action'] .
				'&modfunc=lend_submit" method="POST">';

			$div = $allow_na = false;

			$required = true;

			$document = LibraryGetDocument( $_REQUEST['id'] );

			$current_user_RET = DBGet( DBQuery( "SELECT " . DisplayNameSQL() . " AS FULL_NAME
				FROM " . ( $_REQUEST['type'] === 'student' ? 'STUDENTS' : 'STAFF' ) .
				" WHERE " .
				( $_REQUEST['type'] === 'student' ?
					"STUDENT_ID='" . UserStudentID() . "'" :
					"STAFF_ID='" . UserStaffID() . "'" ) ) );

			echo '<table><tr><td><p>' . sprintf(
				dgettext( 'Library', 'Lend "%s" to %s' ),
				$document['TITLE'],
				$current_user_RET[1]['FULL_NAME']
			) . '</p></td></tr>';

			echo '<tr><td>' . DateInput(
				DBDate(),
				'values[DATE_BEGIN]',
				_( 'Date' ),
				$div,
				$allow_na,
				$required
			) . '</td></tr>';

			echo '<tr><td>' . DateInput(
				// Defaults to today + 1 month.
				date( 'Y-m-d', time() + 60 * 60 * 24 * 30 ),
				'values[DATE_DUE]',
				_( 'Due Date' ),
				$div,
				$allow_na,
				$required
			) . '</td></tr>';

			echo '<tr><td>' . TextInput(
				'',
				'values[COMMENTS]',
				_( 'Comments' ),
				'maxlength="1000" size="30"',
				$div
			) . '</td></tr></table>';

			echo '<br /><div class="center">' . SubmitButton( _( 'Submit' ) ) . '</div>';

			echo '</form>';

			PopTable( 'footer' );
		}
	}
	else
	{
		// Unset modfunc & redirect URL.
		RedirectURL( 'modfunc' );
	}
}

if ( ! $_REQUEST['modfunc'] )
{
	echo ErrorMessage( $error );

	$RET = array();

	// ADDING & EDITING FORM.
	if ( ! empty( $_REQUEST['id'] )
		&& $_REQUEST['id'] !== 'new' )
	{
		$RET = LibraryGetDocument( $_REQUEST['id'] );

		$title = LibraryMakeDocumentAPA( $_REQUEST['id'] );

		// Set Document Category ID if not set yet.
		if ( empty( $_REQUEST['category_id'] ) )
		{
			$_REQUEST['category_id'] =  $RET['CATEGORY_ID'];
		}
	}
	elseif ( ! empty( $_REQUEST['category_id'] )
		&& $_REQUEST['category_id'] !== 'new'
		&& empty( $_REQUEST['id'] ) )
	{
		$RET = DBGet( DBQuery( "SELECT ID AS CATEGORY_ID,TITLE,SORT_ORDER
			FROM LIBRARY_CATEGORIES
			WHERE ID='" . $_REQUEST['category_id'] . "'" ) );

		$RET = $RET[1];

		$title = $RET['TITLE'];
	}
	elseif ( ! empty( $_REQUEST['id'] )
		&& $_REQUEST['id'] === 'new' )
	{
		$title = dgettext( 'Library', 'New Document' );

		$RET['ID'] = 'new';

		$RET['CATEGORY_ID'] = isset( $_REQUEST['category_id'] ) ? $_REQUEST['category_id'] : null;
	}
	elseif ( $_REQUEST['category_id'] === 'new' )
	{
		$title = dgettext( 'Library',  'New Document Category' );

		$RET['CATEGORY_ID'] = 'new';
	}

	echo LibraryGetDocumentsForm(
		$title,
		$RET,
		isset( $extra_fields ) ? $extra_fields : array()
	);

	echo LibraryGetDocumentLoansHeader( $RET );

	// CATEGORIES.
	$categories_RET = DBGet( DBQuery( "SELECT ID,TITLE,SORT_ORDER
		FROM LIBRARY_CATEGORIES
		ORDER BY SORT_ORDER,TITLE" ) );

	// DISPLAY THE MENU.
	echo '<div class="st">';

	LibraryDocumentsMenuOutput( $categories_RET, $_REQUEST['category_id'] );

	echo '</div>';

	// DOCUMENTS.
	if ( ! empty( $_REQUEST['category_id'] )
		&& $_REQUEST['category_id'] !=='new'
		&& $categories_RET )
	{
		$documents_RET = DBGet( DBQuery( "SELECT ID,TITLE,AUTHOR,YEAR,REF
			FROM LIBRARY_DOCUMENTS
			WHERE CATEGORY_ID='" . $_REQUEST['category_id'] . /*"'
			AND SCHOOL_ID='" . UserSchool() . */"'
			ORDER BY TITLE" ),
		array(
			'TITLE' => 'LibraryMakeDocumentAPA',
		) );

		echo '<div class="st">';

		LibraryDocumentsMenuOutput( $documents_RET, $_REQUEST['id'], $_REQUEST['category_id'] );

		echo '</div>';
	}
}


